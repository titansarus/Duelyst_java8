package Duelyst.Model.Battle;

public enum KindOfActionForValidCells {
    MOVE, ATTACK, INSERT, ITEM, SPELL
}
