package Duelyst.Model.Battle;

public enum BattleRecordEnum { //NOT USED IN PHASE 2
    MOVE, END_TURN,
    ATTACK,
    COUNTER_ATTACK,
    HERO_USE_SP,
    INITIALIZE,
    DEATH,
    END_GAME,
    INSERT,
    INSERT_FLAG ,
    INSERT_ITEM,
    MANA_CHANGE;

}
