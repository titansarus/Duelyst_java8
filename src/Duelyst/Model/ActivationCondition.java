package Duelyst.Model;

public enum ActivationCondition {
    ON_SPAWN, PASSIVE, ON_DEATH, ON_ATTACK, ON_DEFEND;
}
