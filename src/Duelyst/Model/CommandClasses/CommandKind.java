package Duelyst.Model.CommandClasses;

public enum CommandKind {
    LOGIN, SHOP, BATTLE,CHAT_ROOM,LEADER_BOARD,ONLINE_PLAYERS,CUSTOM_CARD,TV;
}
