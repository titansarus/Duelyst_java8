package Duelyst.Model;

import java.util.ArrayList;


import java.util.ArrayList;
import java.util.Collections;

public class Warrior extends Card implements Cloneable {
    //TODO CLONE

    private int healthPoint;
    private int actionPower;
    private int attackRange;
    private AttackKind attackKind;
    private boolean validCounterAttack = true;
    private boolean isDeath;
    private int shield;
    private boolean isValidToAttack = true;
    private boolean IsValidToMove = true;
    private boolean isStun = false;


    public Warrior(String cardName, String cardDescription, int manaCost, int darikCost) {
        super(cardName, cardDescription, manaCost, darikCost);
    }

    public Warrior(String cardName, String cardDescription, int manaCost, int darikCost, int healthPoint, int actionPower, int attackRange, AttackKind attackKind, int shield) {
        super(cardName, cardDescription, manaCost, darikCost);
        this.healthPoint = healthPoint;
        this.actionPower = actionPower;
        this.attackRange = attackRange;
        this.attackKind = attackKind;
        this.shield = shield;
    }

    public Warrior(String cardName, String cardDescription, int manaCost, int darikCost, int healthPoint, int actionPower, int attackRange, AttackKind attackKind, int shield, String addressOfImage) {
        super(cardName, cardDescription, manaCost, darikCost, addressOfImage);
        this.healthPoint = healthPoint;
        this.actionPower = actionPower;
        this.attackRange = attackRange;
        this.attackKind = attackKind;
        this.validCounterAttack = validCounterAttack;
        this.isDeath = isDeath;
        this.shield = shield;
    }

    public Warrior(String cardName, String cardDescription, int manaCost, int darikCost, String addressOfImage, String addressOfIdleGif, int healthPoint, int actionPower, int attackRange, AttackKind attackKind, int shield) {
        super(cardName, cardDescription, manaCost, darikCost, addressOfImage, addressOfIdleGif);
        this.healthPoint = healthPoint;
        this.actionPower = actionPower;
        this.attackRange = attackRange;
        this.attackKind = attackKind;
        this.shield = shield;
    }

    public Warrior(String cardName, String cardDescription, int manaCost, int darikCost, String addressOfImage, String addressOfIdleGif, String addressOfRunGif, String addressOfAttackGif, int healthPoint, int actionPower, int attackRange, AttackKind attackKind, int shield) {
        super(cardName, cardDescription, manaCost, darikCost, addressOfImage, addressOfIdleGif, addressOfRunGif, addressOfAttackGif);
        this.healthPoint = healthPoint;
        this.actionPower = actionPower;
        this.attackRange = attackRange;
        this.attackKind = attackKind;
        this.shield = shield;
    }

    public Warrior(String cardName, String cardDescription, int manaCost, int darikCost, String addressOfImage, String addressOfIdleGif, String addressOfRunGif, String addressOfAttackGif, String addressOfGetDamageGif, String addressOfDeathGif,
                   int healthPoint, int actionPower, int attackRange, AttackKind attackKind, int shield) {
        super(cardName, cardDescription, manaCost, darikCost, addressOfImage, addressOfIdleGif, addressOfRunGif, addressOfAttackGif, addressOfGetDamageGif, addressOfDeathGif);
        this.healthPoint = healthPoint;
        this.actionPower = actionPower;
        this.attackRange = attackRange;
        this.attackKind = attackKind;
        this.shield = shield;
    }

    public void changeShield(int i) {
        setShield(getShield() + i);
    }

    public void decreaseShield(int i) {
        setShield(getShield() - i);
    }

    public void increaseShield(int i) {
        setShield(getShield() + i);
    }

    public void setShield(int shield) {
        this.shield = shield;
    }

    public int getHealthPoint() {
        return healthPoint;
    }

    public void increaseHealthPoint(int number) {
        healthPoint += number;
    }

    public void decreaseHealthPoint(int number) {
        healthPoint -= number;
    }

    public int getActionPower() {
        return actionPower;
    }

    public void increaseActionPower(int number) {
        actionPower += number;
    }

    public void decreaseActionPower(int number) {
        actionPower -= number;
    }

    private void checkDeath() {
        if (healthPoint <= 0)
            isDeath = true;
    }

    public boolean isDeath() {
        checkDeath();
        return isDeath;
    }

    public int getShield() {
        return shield;
    }

    public int getAttackRange() {
        return attackRange;
    }


    public boolean isValidCounterAttack() {
        return validCounterAttack;
    }

    public void setValidCounterAttack(boolean validCounterAttack) {
        this.validCounterAttack = validCounterAttack;
    }


    public boolean isValidToAttack() {
        return isValidToAttack;
    }

    public void setValidToAttack(boolean validToAttack) {
        isValidToAttack = validToAttack;
    }

    public boolean isValidToMove() {
        return IsValidToMove;
    }

    public void setValidToMove(boolean validToMove) {
        this.IsValidToMove = validToMove;
    }

    public AttackKind getAttackKind() {
        return attackKind;
    }

    public boolean isStun() {
        return isStun;
    }

    public void setStun(boolean stun) {
        isStun = stun;
    }


    //TODO MAYBE THIS NEEDS TO BE IMPLEMENTED HERE. THIS CLONE IS CURRENTLY Implemented IN CARD
    /*@Override
    protected Object clone() throws CloneNotSupportedException {
        Warrior warrior = (Warrior) super.clone();
        ArrayList<ABuff> buffsClone = ABuff.aBuffClone(this.getBuffs());
        warrior.setBuffs(buffsClone);
        return warrior;
    }*/
}


