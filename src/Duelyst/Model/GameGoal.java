package Duelyst.Model;

public enum GameGoal {
    KILL_HERO, HOLD_FLAG, COLLECT_FLAG

}
